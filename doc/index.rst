:program:`calorine`
*******************

:program:`calorine` is a Python library for constructing and sampling neuroevolution potentials (NEPs) via `GPUMD <https://gpumd.org/>`_.
It provides ASE calculators, IO functions for reading and writing :program:`GPUMD` input and output files, as well as a Python interface that allows inspection NEP models.


.. toctree::
   :maxdepth: 2
   :caption: Main

   installation

.. toctree::
   :maxdepth: 2
   :caption: Tutorials

   tutorials/calculators
   tutorials/nep_descriptors
   tutorials/nep_model_inspection
   tutorials/structure_relaxation
   tutorials/visualize_descriptor_space_with_pca

.. toctree::
   :maxdepth: 2
   :caption: Function reference

   calculators
   gpumd
   nep
   tools

.. toctree::
   :maxdepth: 2
   :caption: Backmatter

   genindex
