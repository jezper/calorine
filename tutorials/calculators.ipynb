{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "29a77281-d15f-4459-b42b-90639d9fd158",
   "metadata": {},
   "source": [
    "# ASE calculators\n",
    "\n",
    "This tutorial demonstrates how to use the [ASE](https://wiki.fysik.dtu.dk/ase/) calculators for neuroevolution potentials (NEP), which allow one to calculate energies, forces and stresses for an atomic configuration, specified in the form of an [Atoms object](https://wiki.fysik.dtu.dk/ase/ase/atoms.html).\n",
    "This enables one to programmatically calculate properties that would otherwise require writing a large number of GPUMD input files and potentially tedious extraction of the results.\n",
    "\n",
    "`calorine` provides two different ASE calculators for NEP calculations, one that uses the GPU implementation and one that uses the CPU implementation of NEP.\n",
    "For smaller calculations the CPU calculators is usually more performant.\n",
    "For very large simulations and for comparison the GPU calculator can be useful as well."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "e69f41d6",
   "metadata": {
    "tags": []
   },
   "source": [
    "## CPU-based calculator"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "0fb133d3",
   "metadata": {},
   "source": [
    "### Basic usage\n",
    "\n",
    "First we define an atomic structure in the form of an [Atoms object](https://wiki.fysik.dtu.dk/ase/ase/atoms.html).\n",
    "Next we create a calculator instance by specifying the path to a NEP model file in [nep.txt format](https://gpumd.zheyongfan.org/index.php/The_output_files_for_the_nep_executable#The_nep.txt_file).\n",
    "In this tutorial, we use a [NEP3 model for PbTe](https://gitlab.com/brucefan1983/nep-data/-/tree/main/PbTe).\n",
    "Finally, we attach the calculator to the `Atoms` object."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "id": "ccb1d625",
   "metadata": {},
   "outputs": [],
   "source": [
    "from ase.build import bulk\n",
    "from calorine.calculators import CPUNEP\n",
    "\n",
    "structure = bulk('PbTe', crystalstructure='rocksalt', a=6.7)\n",
    "calc = CPUNEP('PbTe_NEP3.txt')\n",
    "structure.calc = calc"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "506e961e",
   "metadata": {},
   "source": [
    "Now, we can readily calculate energies, forces and stresses."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "id": "12dbc5b2",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Energy (eV): -7.681070759348718\n",
      "Forces (eV/Å):\n",
      " [[ 2.70616862e-16  2.85239922e-17 -1.16068684e-16]\n",
      " [-2.70616862e-16 -2.85239922e-17  1.16068684e-16]]\n",
      "Stress (GPa):\n",
      " [ 4.82127071e-03  4.82127071e-03  4.82127071e-03  8.90760229e-19\n",
      " -2.20443216e-18  1.01009982e-17]\n"
     ]
    }
   ],
   "source": [
    "print('Energy (eV):', structure.get_potential_energy())\n",
    "print('Forces (eV/Å):\\n', structure.get_forces())\n",
    "print('Stress (GPa):\\n', structure.get_stress())"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "81f94877",
   "metadata": {},
   "source": [
    "### Calculate energy-volume curve\n",
    "\n",
    "To demonstrate the capabilities of the ASE calculator, we will now calculate an energy-volume curve with the PbTe potential."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "id": "861b6acb",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "image/png": "iVBORw0KGgoAAAANSUhEUgAAAZAAAAEICAYAAABxiqLiAAAAOXRFWHRTb2Z0d2FyZQBNYXRwbG90bGliIHZlcnNpb24zLjUuMSwgaHR0cHM6Ly9tYXRwbG90bGliLm9yZy/YYfK9AAAACXBIWXMAAAsTAAALEwEAmpwYAAA0jElEQVR4nO3deXxU5fX48c9JAiSsYYeEHUNQ2QIRWSwWQVG0EpYqrthF1Na1/dKv1GptrV9R1P5sq1ZcqRsqsli0AuJWRdSwyCKbKEtCgLCELSHr+f0xNzqEmTAZMrmznPfrNa/M3eae+yLkzHOf+5xHVBVjjDGmpuLcDsAYY0xksgRijDEmKJZAjDHGBMUSiDHGmKBYAjHGGBMUSyDGGGOCkuB2AHWpVatW2qVLF7fDMMaYiLJ8+fK9qtq66vqYSiBdunQhOzvb7TCMMSaiiMg2X+vtFpYxxpigWAIxxhgTFEsgxhhjgmIJxBhjTFAsgRhjjAmKK09hich9wBigAtgDXKeqO33stxU4DJQDZaqa6axvAbwGdAG2Apep6oFQxDpvZS7TF25kZ0ERKclJTBmVTlZGaihOZYwxEcWtFsh0Ve2jqv2ABcA91ew7XFX7VSYPx53AElVNA5Y4y7Vu3spcps5ZQ25BEQrkFhQxdc4a5q3MDcXpjDEmoriSQFT1kNdiI6Cmk5KMAWY672cCWbUQ1gmmL9xIUWn5ceuKSsuZvnBjKE5njDERxbWBhCJyP3AtcBAY7mc3BRaJiAJPqeoMZ31bVc0DUNU8EWkTihh3FhTVaL0xxsSSkLVAROQ9EVnr4zUGQFXvUtWOwMvAzX4+Zqiq9gcuAn4tIsOCiGOyiGSLSHZ+fn6Njk1JTqrRemOMiSUhSyCqOlJVe/l4za+y6yvAeD+fsdP5uQeYCwx0Nu0WkfYAzs891cQxQ1UzVTWzdesTSrlUa8qodJLqxR+3LrFeHFNGpdfoc4wxJhq50gciImlei5cCG3zs00hEmlS+By4A1jqb3wImOe8nAVWTUq3IykjlgXG9SfVqcVx1dmd7CssYY3CvD2SaiKTjeYx3G3AjgIikAM+o6migLTBXRCrjfEVV3608HnhdRH4BbAd+GqpAszJSycpIpay8gkEPLLH+D2OMcbiSQFS1ultWo5333wJ9/ey3DxgRsgB9SIiP49K+qby0bBsHC0tp1rBeXZ7eGGPCjo1Er4Fx/VMpKa9gwZoTxjwaY0zMsQRSA2emNKVH28bMWWEDCY0xxhJIDYgI4/p3YPm2A2zde9TtcIwxxlWWQGooq18qIjDHypkYY2KcJZAaatcskXNOa8XclTmo1rQCizHGRA9LIEEYm5HKjv1FZG8LSQFgY4yJCJZAgjDqzHY0rB/PnBU5bodijDGusQQShEYNEriwVzsWrM7jWJVqvcYYEyssgQRpXEYHDh8rY8l6v2W4jDEmqlkCCdLg7i1p1zTRbmMZY2KWJZAgxccJWRmpfLgpn71Hit0Oxxhj6pwlkFMwrn8q5RXKW6ustIkxJvZYAjkFPdo2oVdqU+baoEJjTAyyBHKKxmV0YE3uQTbtPux2KMYYU6csgZyiS/ulEB8nVmDRGBNzLIGcolaNG/DjHq2ZvyqX8gorbWKMiR2WQGrB2P6p5B08xrJv97kdijHG1BlLILVg5OltaZKYwJs2JsQYE0MsgdSCxHrxXNKnPe+u3UVhSZnb4RhjTJ2wBFJLxmZ0oLCknIXrdrkdijHG1AlXEoiI3Cciq0VklYgsEpEUP/ttFZE1zn7ZXuvvFZFcZ/0qERldd9H7ltm5OR1bJNnTWMaYmOFWC2S6qvZR1X7AAuCeavYdrqr9VDWzyvq/Ouv7qeo7IYs0QHFxwtiMDnzyzV52HTzmdjjGGBNyriQQVT3ktdgIiIrnX8dlpKIK81ZZK8QYE/1c6wMRkftFZAdwFf5bIAosEpHlIjK5yrabndtgz4lI85AGG6AurRrRuUUSDy/cSNc732botPeZZ2VOjDFRKmQJRETeE5G1Pl5jAFT1LlXtCLwM3OznY4aqan/gIuDXIjLMWf8k0B3oB+QBj1QTx2QRyRaR7Pz8/Fq6Ot/mrcxl58FjlFUoCuQWFDF1zhpLIsaYqCSq7t49EpHOwNuq2usk+90LHFHVh6us7wIsONnxAJmZmZqdnX2y3YI2dNr75BYUnbA+NTmJT+88L2TnNcaYUBKR5T76oV17CivNa/FSYIOPfRqJSJPK98AFwFpnub3XrmMr17ttp4/kUd16Y4yJZAkunXeaiKQDFcA24EYA53HeZ1R1NNAWmCsilXG+oqrvOsc/JCL98PSRbAVuqNPo/UhJTvLZAklJTnIhGmOMCS1XEoiqjvezficw2nn/LdDXz37XhC664E0Zlc7UOWsoKi3/fl1SvXimjEp3MSpjjAkNt1ogUSkrIxWA6Qs3ft8SueP8tO/XG2NMNLEEUsuyMlLJykhlz+FjDHngffYdKXE7JGOMCQmrhRUibZokMuL0NsxenkNJWYXb4RhjTK2zBBJCEwd2Yt/REhZ/vdvtUIwxptZZAgmhYWmtSU1OYtaX290OxRhjap0lkBCKjxMuy+zIfzfvZfu+QrfDMcaYWmUJJMQuO6sDcQKvZVsrxBgTXSyBhFj7ZkkMT2/D69k5lJZbZ7oxJnpYAqkDVwzsRP7hYt7fsMftUIwxptZYAqkDP05vTdumDXj1C7uNZYyJHpZA6kBCfByXZ3bko035PmtlGWNMJLIEUkcuO6sjAK99ucPlSIwxpnZYAqkjHZo3ZFhaa97I3kGZdaYbY6KAJZA6dMXAjuQdPMZHm0I7M6IxxtQFSyB1aMTpbWnVuAGvfmG3sYwxkc8SSB2qFx/HTzM78MHGPew6eMztcIwx5pRYAqljE8/qSHmF8ka2tUKMMZHNEkgd69yyEUNPa8msL3dQUaFuh2OMMUGzBOKCiWd1IregiP9+s9ftUIwxJmiWQFxwwZltadGoPrNsZLoxJoK5kkBE5D4RWS0iq0RkkYik+NkvWURmi8gGEVkvIoOd9S1EZLGIbHZ+Nq/bKzg1DRLiGd8/lcVf7yb/cLHb4RhjTFACTiAi0khE4mvpvNNVtY+q9gMWAPf42e8x4F1V7Qn0BdY76+8ElqhqGrDEWY4oEwd2oqxCmb08x+1QjDEmKH4TiIjEiciVIvK2iOwBNgB5IrJORKaLSFqwJ1XVQ16LjYATepNFpCkwDHjWOaZEVQuczWOAmc77mUBWsLG4pXvrxgzs2oJZX263znRjTMjMW5nL0Gnv0/XOtxk67X3mrcyttc+urgXyAdAdmAq0U9WOqtoG+BGwDJgmIlcHe2IRuV9EdgBX4bsF0g3IB54XkZUi8oyINHK2tVXVPADnZ5tqzjNZRLJFJDs/P7xGgF85sBPb9hWy7Nt9bodijIlC81bmMnXOanILilAgt6CIqXPW1FoSEVXf335FpJ6qllZ7cDX7iMh7QDsfm+5S1fle+00FElX1j1WOz8STqIaq6uci8hhwSFXvFpECVU322veAqp60HyQzM1Ozs7NPtludOVZaTsafFznvK0hJTmLKqHSyMlJdjswYEw2GTFvCzoITBy2nJifx6Z3nBfw5IrJcVTOrrk/wd4B3YnA6qTt676+qK6pLMKo6MsDYXgHeBv5YZX0OkKOqnzvLs/mhr2O3iLRX1TwRaQ9E5ExN767dRUm5Uu7cwqr8dgBYEjHGnJKDRaU+kwfAzlqaVuKknegich+wGvgb8IjzevhUTlql/+RSPP0rx1HVXcAOEUl3Vo0AvnbevwVMct5PAuYTgaYv3Ph98qhUVFrO9IUbXYrIGBMNduwvZMKTS/1uT0lOqpXz+G2BeLkM6K6qJbVyRo9pTmKoALYBNwI4j/M+o6qjnf1uAV4WkfrAt8DPKo8HXheRXwDbgZ/WYmx1xt+3gNr6dmCMiT0rth9g8r+yKSmr4FfDu/P8J1spKi3/fntSvXimjEqv5hMCF0gCWQskU4u3iVR1vJ/1O4HRXsurgBPuu6nqPjwtkoiWkpzkc4bC2vp2YIyJLQtW7+S3r39F26aJzJp8Fqe1aUyPNk2YvnAjOwuKar2fNZAE8gCwUkTWAt+PelPVS2slghg2ZVQ6U+esCdm3A2NMbFBVnvhwC9MXbiSzc3NmXJtJi0b1AU9/aqj6VANJIDOBB4E1eG45mVpS+Y86feHG71sit5+fZh3oxpiAlZRVcNfcNbyxPIcx/VJ4cHwfEuvV1pjv6gWSQPaq6t9CHkmMqvx2kH+4mKHT3ifPz1MTxhhTVUFhCTe+tJxl3+7nthFp3D4yDRGps/MHUspkuYg8ICKDRaR/5SvkkcWY1k0acHGf9sxensOR4jK3wzHGhLmte48y7omlrNhWwF8v78sd5/eo0+QBgbVAMpyfg7zWKRD4KBQTkElDujB3ZS5vLs9h0pAubodjjAlTX27dz+R/eQZFv/TLsxnYtYUrcZw0gajq8LoIxEC/jsn07ZjMzM+2cs2gzsTF1e23CWNM+Ju/Kpcpb6wmtXkSz193Fl1aNTr5QSESyEDCZiLyaGU9KRF5RESa1UVwsei6IZ35Nv8on9hkU8YYL6rK/3tvE7fNWkVGp2Tm/mqIq8kDAruF9RyesSCXOcvXAM8D40IVVCwb3bs997+9nplLtzKsR2u3wzHGuGTeytzvx2+0b5ZISnIi2dsKGN+/Aw+M6039BPfnAwwkgXSvMvDvTyKyKkTxxLwGCfFcObATf//gG7bvK6RTy4Zuh2SMqWOeKro/jBHbefAYOw8eY3Svdjz80z513lnuTyAprEhEzqlcEJGhgNXaCKGrBnUmXoR/fbbV7VCMMS6YvnDjcQOMK32VczBskgcElkBuBB4Xka0ishX4B3BDSKOKcW2bJnJhr3a8nr2DwhJ7pNeYWBMpdfICSSCHVLUv0Afoo6oZwOHQhmWuG9KFQ8fKmFuLs4cZYyJDZRmSqsKtTl4gCeRN8ExD6zUV7ezQhWQABnRuzpkpTZm5dCv+Jv0yxkSf17/cwf6jJVS9UxWOdfKqmxO9p4iMB5qJyDiv13VAYp1FGKNEhElDurBp9xE+sylvjYl6qso/3t/M795czTlprXhgbG9Sk5MQPDMIPjCud9jVyavuKax04BI8pdx/4rX+MHB9CGMyjkv7pvDAO55Heod0b+V2OMaYECmvUP7073X867NtZPVL4aEJfamfEMfEgZ3cDq1a1U1pOx+YLyKDVfWzOozJOBLrxTNxYCee+mgLOQcK6dDcHuk1JtocKy3njtdW8Z+1u5g8rBt3XtgzYqpQBNIHslJEfi0iT4jIc5WvkEdmALh6UGcAXlq23eVIjDG17WBRKdc+9wX/WbuLP1x8Or8ffXrEJA8ILIG8CLQDRgEfAR2wp7DqTGpyEhec0Y5ZX27nmI/nwo0xkWnXwWNc/tRnrNx+gMcm9uOXP+rmdkg1FkgCOU1V7waOqupM4GKgd2jDMt4mDelCQWEpb63a6XYoxpha8M2ew4x/cik79hfy/HUDGdMvvDrHAxVIAil1fhaISC+gGdDlVE4qIveJyGoRWSUii0Qkxc9+ySIyW0Q2iMh6ERnsrL9XRHKd41eJyGhfx0eLQd1akN62CS/YI73GRLzl2w4w4Z+fUVxWwWs3DOactMh9QCaQBDJDRJoDdwNvAV/jmeL2VExX1T6q2g9YANzjZ7/HgHdVtSfQF1jvte2vqtrPeb1zivGEtcpHer/OO0T2tgNuh2OMCdJ7X+/mqmeWkZxUjzk3DaFXamQXNq9uHMhgERFVfUZVD6jqR6raTVXbqOpTp3JSrwGJAI3wTFBV9fxNgWHAs84xJapacCrnjWRZGSk0TUzghaVb3Q7FGBOEWV9sZ/KL2fRo24TZNw2JikKp1bVAJuGZznaWiFwnIu1q88Qicr+I7ACuwncLpBuQDzwvIitF5BkR8S5+f7NzG+w5p4UU1RrWT+Dyszry7tpd7Dpo86YbEylUlb8v2cydc9ZwTlprXr1+EK0aN3A7rFohJ7unLiI9gYvwPIXVDPgAeBf4VFX9PhYkIu/heXqrqrucMSaV+00FElX1j1WOzwSWAUNV9XMReQxPXa67RaQtsBdPy+U+oL2q/txPHJOByQCdOnUasG3btmqvN5xt31fIuQ9/wM3DT+O3F4RXSQNjzA+85/JIqh9PYUk54zJSeXBCH+rFuz+PR02JyHJVzTxhfU06ZUUkCRiOJ6EM9vWBQQTWGXhbVXtVWd8OWKaqXZzlHwF3qurFVfbrAiyoerwvmZmZmp2dfaohu+riv33M+rzDqHoKq00ZlR525Q2MiWVV5/IASIgTpk/ow9j+HVyMLHj+Ekh1fSBvi8hV3reNVLVIVd9R1VtOJXmISJrX4qXAhqr7qOouYIeIVH7VHoGnAx8Rae+161g8MyZGvXkrc9m8+ygV6ml65RYUMXXOGuZZxV5jwoavuTzKKpSHF21yKaLQqa4tNQNPLazvROQ1EckSEd81hmtumoisFZHVwAXAbQAikiIi3k9U3QK87OzXD/g/Z/1DIrLGWT8cuKOW4gpr0xdupKS84rh1RaXlTF+40aWIjDFVRcpcHrUhkFpYSXhaCZOAfzp/4F9V1cXBnrTKFLne63cCo72WVwEntHRU9Zpgzx3JYukX05hI9N3eo8TFCeUVJ3YNhNtcHrXhpL05zm2r11R1LJ7WQgaeTnRTx/z9AkbjL6YxkWZNzkEmPLmUxIQ4GiQc/6c1HOfyqA0nTSAi0lZEbhGRT4F5wCJgQKgDMyeaMiqdpHrxx61rkBAXlb+YxkSST7/Zy8QZn5FYL563bjmHB8f3Cfu5PGqD31tYInI9cAWeeUHmAL9T1U/rKjBzospfwMrHAwF6pzaNyl9MYyLF26vzuOO1VXRt1YiZPx9Iu2aJdG/dOCb+X1Y3odQQYBrwnqpWVLOfqUNZGanf/2Let+BrXli61eYKMcYlLy7bxj3z1zKgU3OenXQWzRrWczukOuX3Fpaq/kxVFwEqIleLyD0AItJJRAbWWYTGr5+f0xUBnvtkq9uhGBNTVJW/Lt7E3fPWcl56G178xdkxlzwgsGKKTwCD8dzOAs9cII+HLCITsNTkJC7tm8KsL7dTUFjidjjGxITyCuXu+Wt5bMlmJgzowFPXDCCpfvzJD4xCgSSQs1X118AxAFU9ANTWeBBziiaf243CknJeWha5JVqMiRTFZeXc8uoKXlq2nRvO7cb0CX1IiMDSJLUloPlARCQep2KuiLQGrE8kTPRs15Qfp7fmhaVbbcZCY0Lo8LFSfvb8l7yzxjP97NSLTkckcqafDYVAEsjfgLlAGxG5H/iEH0aEmzBw47nd2XukhNnLc9wOxZiolH+4mCueXsYX3+3n0cv6RuT0s6FQ3VNYAKjqyyKyHE8tKgGyVHX9SQ4zdejsri3o2zGZp//7LVcM7ER8XGx/KzKmNu3YX8g1z37OrkPHeHpSJsPT27gdUtiobhxIY1U9AqCqG/BR8NB7H+MeEeHGYd246eUVLFy3i9G925/8IGOMT96l2Fs3aUBRSRlxcXG8/MtBDOgc9VMP1Uh1t7Dmi8gjIjLMuyKviHQTkV+IyELgwtCHaAJxwZnt6NKyIU99tMXmTTcmSJWl2HMLilBgz+FiDheXc+O53Sx5+FDdOJARwBLgBmCdiBwSkX3AS3gmipqkqrPrJkxzMvFxwvXDuvFVzkGWfbvf7XCMiUi+SrEDvLRsuwvRhL9q+0BU9R3gner2MeFjfP8O/HXxJp76eAuDu7d0OxxjIo5VvK6Z2H2AOQol1ovnuiFd+HBjPuvzDrkdjjERRVVp3MD3d2qreO2bJZAoc/WgzjSsH8+Mj791OxRjIkZpeQX/88ZqDheXnfAUY7SWYq8NlkCiTHLD+lwxsBNvfbWTnAOFbodjTNg7UlzGL2Zm8+aKHO4Y2YOHJ8RGKfbacNJxICLyMPC8qq6rg3hMLfj5OV2ZuXQrz32ylXt+cobb4RgTtvYcPsbPX/iS9XmHeXB8by4/qxMAY/t3cDmyyBBIC2QDMENEPheRG0WkWaiDMqfGiiwac3Lf5h9h/JNL2bLnKM9cm/l98jCBC2RK22dUdShwLdAFWC0ir4jI8FAHZ4JnRRaN8W/F9gOMf3IphcXlzJo8iOE9bXR5MALqA3GKKfZ0XnuBr4DfiMisYE4qIveJyGoRWSUii0Qkxcc+6c72ytchEbnd2dZCRBaLyGbnp43wqcKKLBrj23tf7+bKp5fRNKkeb940hL4dk90OKWIFMif6o8BGYDTwf6o6QFUfVNWfABlBnne6qvZR1X7AAuCeqjuo6kZV7efsMwAoxFPUEeBOYImqpuEZ7HhnkHFEtRuGWZFFY7y9/Pk2Jr+YTY+2TXjzpiF0adXo5AcZvwJpgawF+qjqDar6RZVtQc1MqKregxQa4ZSKr8YIYIuqVt6PGQPMdN7PBLKCiSPaDerWgr4dmvH0f7+lvMLKm5jYpao8umgjd81dy7k9WvPq9YNo1biB22FFvJM+hQWsAnpWqXt/ENimqgeDPbFTGv5a57NO1p8yEXjVa7mtquYBqGqeiNgNTB9EhBvO7c6vXl7BWX9ZzIHCUlKSk5gyKt0eSzQxo7S8gt/PWcMby3O4PLMj94/tFdOTQNWmQKe0XQbMAJ4GPgNmAZtE5AJ/B4nIeyKy1sdrDICq3qWqHYGXgZur+Zz6wKXAGwFf1fHHTxaRbBHJzs/PD+YjItqxknIE2F9YigK5BUVMnbOGeStz3Q7NmJA7WlzG9f/K5o3lOdw2Io1p43tb8qhFgbRAtgK/qBwHIiJnAFOA+4A5wCJfB6nqyABjeAV4G/ijn+0XAStUdbfXut0i0t5pfbQH9vj7cFWdgSf5kZmZGXP3cR5ZvOmE+4NFpeVMX7jRWiEmKnmXY0+IF0rLlQfG9eaKgfaYbm0LJBX39B5EqKpfAxmqGnStDBFJ81q8FB9zjXi5guNvXwG8BUxy3k8C5gcbS7Sz4nAmllQtx15artSPjyOpXrzboUWlQBLIJhF5UkTOdV5POOsaAKVBnneacztrNXABcBuAiKSIyPfVf0WkIXA+npbOcccD54vIZmf7tCDjiHr+isBZcTgTjXyVYy8pr2D6wo0uRRTdArmFNQn4FXA7niltPwH+B0/yCGowoaqO97N+J57HhSuXC4ET6pKr6j48T2aZk5gyKp2pc9Yc958qqV6cFYczUSnXWtx1qtoE4gwg/LfTn/GIj11sOtswV9nPMX3hxu//c105sJP1f5iooqo88eEWv9utxR0aJ5tQqlxECkWk2ak8smvclZWRSlZGKmXlFYx89CM+3bKPigolrkrZamMiUUlZBb+fu4bZy3MY0CmZdXmHOFZa8f12K8ceOoH0gRwD1ojIsyLyt8pXqAMztS8hPo47zu/Bhl2HeWdtntvhGHPKDhaWMum5L5i9PIfbR6Yx+6YhTBtn5djriqhW/2SriEzytV5VZ/paH84yMzM1Ozvb7TBcVV6hXPTYx5RVKItuH2bPxJuItW3fUX72wpfk7C/iwQm9GZthJdhDRUSWq2pm1fUn7URX1ZkikgR0UlV7lCHCxccJd4zswU0vr2D+qp2MH2D/6Uzkyd66n8kvLqdClZd+eTYDu7ZwO6SYFEgxxZ/gKWfyrrPcT0TeCnFcJoRGndmOM1Oa8tiSzZSWV5z8AGPCyPxVuVz59Oc0S6rH3F8NteThokDuX9yLp2hiAYCqrgK6hiwiE3JxccJvL+jB9v2FVqnXRAxV5W9LNnPbrFX065TMnJuG0NWq6boqkARS5uMJrJgrCRJthqe3IaNTMn9bstnmCzFhr7isnN++8RWPLt7EuIxUXvzFQJo3qu92WDEvoHLuInIlEC8iaSLyd2BpiOMyISYi/M8F6eQdPMasL7a7HY4xfhUUlnDNs18wZ0Uuvzm/B49c1pcGCVaaJBwEkkBuAc4EivHUpDqEZ1S6iXBDurfk7K4tePzDLRSVWCvEhJ+te48y9omlrNpewGMT+3HriDSqTC1hXBTIU1iFwF3Oy0QREeG3F6Rz2VOf8eKyrUwe1t3tkEyM866k27JxfQqLy2hQL55Xrj+bzC7WWR5uTppARKQHntpXXbz3V9XzQheWqSsDu7ZgWI/WPPnhFq48uzONGwRSHs2Y2ldZSbeybtveIyUIcMf56ZY8wlQgt7DeAFYCf8AzD0jly0SJ357fgwOFpTz/yXduh2JimK9Kugq8sHSrK/GYkwvk62aZqj4Z8kiMa/p2TGbk6W2Z8d9vuXZwF5o1rOd2SCYG2dw1kSeQFsi/ReRXItJeRFpUvkIemalTvzm/B4ePlfHMJ0HPE2ZM0PYeKaaen7I6Vkk3fAWSQCbhuWW1FFjuvGK7oFQUOiOlKRf3ac9zn3zH/qMlbodjYsjm3YfJevxTKrSC+vHHP2FllXTD20kTiKp29fHqVhfBmbp1x8g0ikrLeeoj//MqGFObPt6Uz7gnllJcVsGbNw3loQl9rZJuBPHbByIiv1PVh5z3P1XVN7y2/Z+q/r4uAjR157Q2Tcjql8rMz7byi3O60qZpotshmSj24rJt3PvWOnq0bcKzkzJJSU6ib8dkSxgRpLoWyESv91OrbLswBLGYMHDbyDRKy6uf3c2YU1Feofzp3+u4e95azu3RmjduHGz9HBGquqewxM97X8smSnRu2YiBXZrzwtKtzFy6lZTkJKaMSrdvhaZWHCku49ZXV/L+hj38fGhX7rr4dOJtZsyIVV0LRP2897VcIyJyn4isFpFVIrJIRFJ87JPubK98HRKR251t94pIrte20acSj/nBvJW5rNheAHj+kXMLipg6Zw3zVua6GpeJfLkFRUx4cikfbcrnL1m9uOcnZ1jyiHB+ZyQUkXLgKJ7WRhJQWLkJSFTVoAcLiEhTVT3kvL8VOENVb6xm/3ggFzhbVbeJyL3AEVV9uCbntRkJT27otPfJ9fHcfWpyEp/eacUHTHBW7SjglzOzKS4t5/Gr+jOsR2u3QzI1UOMZCVU1ZOUuK5OHoxEnb9GMALao6rZQxWQ8bDCXqW3vrMnjjtdW0aZpA169/mzS2jZxOyRTS1wrfCQi9wPXAgeB4SfZfSKeSsDebhaRa/GMSfmtqh6o/ShjT0pyks8WiHVymkB4F0NMSU6kX8dk3l6ziwGdmzPjmgG0bNzA7RBNLQpkIGFQROQ9EVnr4zUGQFXvUtWOwMvAzdV8Tn3gUjw1uSo9CXQH+gF5wCPVHD9ZRLJFJDs/P//ULyzKTRmVTlK9Exuft5xnlXpN9SqLIeYWFDn9Z8d4e80u+ndK5uVfnm3JIwqFrAWiqiMD3PUV4G3gj362XwSsUNXdXp/9/XsReRpYUE0cM4AZ4OkDCTCmmFX5tFXlt8hWjRuQf6SYzXuOuhyZCXe+iiEC7D50jEQfX0pM5HPlFpaIpKnqZmfxUmBDNbtfQZXbVyLSXlXznMWxwNrajzJ2ZWWkHvfY7u/nruGFpVv5aWYHerZr6mJkJpz57z87VseRmLoSsltYJzHNuZ21GrgAuA1ARFJE5J3KnUSkIXA+MKfK8Q+JyBrn+OHAHXUUd0z63ah0miXV4+55a/H31J4xLfzMUW79Z9HLlRaIqo73s34nMNpruRBo6WO/a0IXnakquWF97rywJ797czVvrshlwoAObodkwoiq8uwn37HvaAki4P0dw4ohRje3WiAmwkwY0IH+nZJ54J31HCwsdTscEyZKyir43zdX85e313NRr3Y8OK63FUOMITZ/qQlIXJxwX1YvfvL3T3h40Ubuy+rldkjGZfuOFHPTSyv4Yut+bj3vNG4f2YO4OOGyszq5HZqpI9YCMQE7M6UZ1w7uwkufb2NNzkG3wzEu2rjrMGMe/5Svcgr4+xUZ/OaCdOKsLEnMsQRiauQ3F/SgVeMG/GH+WioqrEM9Fr339W7GPfEpJWUVvH7DYH7S94RSdiZGWAIxNdI0sR53jT6dr3YUMOvLHW6HY+qQqvLPj7Zw/YvZdGvdmLduPoe+HZPdDsu4yBKIqbEx/VIY1K0FDy3cYNPfxojisnJ++8ZXTPvPBkb3bs/rNwymXTObcCzWWQIxNSYi3DemF0eOlfHgf6obA2qiQf7hYq6YsYw5K3K5Y2QP/nFFBkn1bWS5saewTJDS2jbhF+d05amPv+WyszoyoHNzt0MytcS7IGLrJg0oKavgWFk5j1/Zn4v7tHc7PBNGrAVignbriDTaN0vk7nlrKSuvcDscUwuqFkTcc7iYgqJSfv3j0yx5mBNYAjFBa9QggbsvOYOv8w7x0jKbqiUa+CuIaA9MGF8sgZhTclGvdvworRWPLNrEnsNWNC/S2YRipiYsgZhTIiL8eUwvCkvKOPehD+l659sMnfa+zaEegb7NP+J3jnIriGh8sQRiTtlXOwoQEYpKy52JhIqYOmeNJZEIsvjr3Yz5x6c0SIijfvzxfxasIKLxxxKIOWXTF26krMqo9KLScqYv3OhSRCZQFRXKo4s3cf2/sunSqhGLfnMuD03oYwURTUDsMV5zyuy+eWQ6WFTK7bNW8sHGfCYM6MBfsnqRWC+e1CoTihnjjyUQc8pSkpPI9ZEs7L55+Nq46zCTX8wm90AR9405k6sHdUbEiiGamrFbWOaUTRmVTpKPOa+vOLujC9GYk1mweidZj39KYUk5syYP4prBXSx5mKBYAjGnLCsjlQe8JhJq1yyRpokJvP5ljk0+FUbKyiv4v3fWc/MrKzkjpSkLbjmHzC4t3A7LRDCJpTmuMzMzNTs72+0wYsKK7Qe4/KnP+FFaa565NtPminDZ/qMl3PLqCj79Zh9XD+rEPZecSf0E+/5oAiMiy1U1s+p66wMxIdG/U3PuueQM7p6/jn988A23jkhzO6SY4l3PqlWTBpSWVVBYWs5DE/pwWabdWjS1w5WvICJyn4isFpFVIrJIRHzOSCMid4jIOhFZKyKvikiis76FiCwWkc3OT6vkF4auHtSZcRmp/PW9TXy4cY/b4cSMqvWs8g8Xc7ColJuHd7fkYWqVW23Y6araR1X7AQuAe6ruICKpwK1Apqr2AuKBic7mO4ElqpoGLHGWTZgREe4f25v0tk24bdYqduwvdDukmOCrnpUCr32Z405AJmq5kkBU9ZDXYiM8v9++JABJIpIANAR2OuvHADOd9zOBrBCEaWpBUv14nrpmABWq3PTyco75KNRnapeNyzF1xbVeNBG5X0R2AFfhowWiqrnAw8B2IA84qKqLnM1tVTXP2S8PaFM3UZtgdG7ZiP93eT/W5h7i7nlriaUHN+qSqvLK59v9fhuzcTmmtoUsgYjIe07fRdXXGABVvUtVOwIvAzf7OL45npZGVyAFaCQiVwcRx2QRyRaR7Pz8/FO7KBO0Eae35dbzTuON5TlWGjwEDhwt4caXlvP7uWtIa9OIxASrZ2VCL2QJRFVHqmovH6/5VXZ9BRjv4yNGAt+par6qlgJzgCHOtt0i0h7A+em3h1ZVZ6hqpqpmtm7d+tQvzATttpE9GNajNX+cv46vdhS4HU7UWPrNXi567L+8v2EPvx/dk4W3n8u08VbPyoSeK4/xikiaqm52Fi8FfE2svR0YJCINgSJgBFA5iOMtYBIwzflZNSmZMBQfJzx2eT8u+fsn3PTSchbc+iNaNKrvdlgRq6SsgkcXb+Kpj7fQtWUjnpk0lF6pzQDP4E5LGCbU3OoDmebczloNXADcBiAiKSLyDoCqfg7MBlYAa5xYZ1QeD5wvIpuB851lEwGaN6rPP68ewN6jJdz66krKK6w/JBjf5h9h/JNL+edHW5h4VkcW3HrO98nDmLpiI9GNK17/cge/e3M155/ehq/zDrOzoIiU5CSmjEq3b87VUFXeyM7hj2+to35CHA+O782FvWyuchNaNhLdhJXLzurI3JU5LF7/Q/dV5URUgCURHw4WljJ17mreWbOLwd1a8ujlfWnfzJ6sMu6xBGJcs23fiQMLKyeiivUE4l2KJCU5ibEZKby5Ipf8w8X874U9mTysm9/pZ42pK5ZAjGvyDh7zuT7WB7xVliKpHE2eW1DEPz7YQqvG9XnzpiH07ZjsboDGOKwcp3GNv4FtKcmJdRxJePFVigSgXnycJQ8TViyBGNf4m4iqc8uGlJZXuBBRePDXAtvlp8VmjFssgRjXVJ2IKiU5kRE927B0y36uffYLDhwtcTvEOlVaXsHzn37nd7uVIjHhxvpAjKt8DXh7c3kOU+esIeuJT3l2UiantWniUnR15+NN+fx5wdd8s+cIPdo0Ztv+QorLfmiFWSkSE46sBWLCzvgBHXh18tkcLS5j7ONLo3ouke/2HuWXM7/k2ue+oLS8gqevzWThHcN40EqRmAhgAwlN2Mo5UMj1/1rOxl2H+MPFZ/CzoV0QiY5HVw8fK+UfH3zDc598R/34OG4ZkcbPhnahQcKJfULGuM0GEpqI06F5Q2bfOJg7XlvFnxd8zabdh/nzmF4RPZd3RYUye0UOD727kb1HipkwoAO/G5VOm6ax/eSZiUyWQExYa9QggX9ePYBHFm/k8Q+28N3eozx59YCIKcLoPSCwZeMGJNWLY8eBIjI6JfPspEx7LNdENEsgJuzFxQlTRvUkrU0TfvfmakY++iEJcXHkHy4O6/pZVQcE7j1SDMDVgzrx50t7EWcjyU2Ei9x7ASbmZGWk8qsfd2f/0VL2HC5G+aF+1ryVuW6Hd5xjpeX8+d/rfA4I/GBDviUPExWsBWIiyhvZOSesKyot54H/rHe9FVJRoXz+3X7mrczlnTV5HC4u87lfrJdqMdHDEoiJKP7++O4+VMxP/7mUrIxULu7dnuSGdddHsmn3YeauzGX+ylx2HjxGo/rxXNirPR9u3MM+H4MhbUCgiRaWQExESUlOItdHEmmamMCBwlLumruWe99ax/D0NozNSGV4zzYk+iiXUhNVK+NOGZXOkO4tmb9qJ3NX5vJ13iHi44Rhaa3434t6csEZ7UiqH39CHwjYgEATXWwciIko/v4oPzCuN2P6pbBu5yHmrcxl/lc7yT9cTJPEBC7u3Z6sjFTyDhTx8OJNNZq8ytf54gQqJ1Ls26EZYzNSuaRvCq0aN/B5fNXk4/atNmNqyt84EEsgJuIE8ke5vEJZumUvc1fm8u7aXRSWlCOA9297vXghKyOV9LZNKCwp52hJGYXFnp9FJeUcLSln2ZZ9lPgo7NikQQLzbh5K99aNQ3uxxoQBSyBYAolVhSVlDJ32PgcKS6vdr0FCHA3rx9OwfgKNGnh+rtpR4HNfAb6bdnHtB2tMGLKR6CZmNayfQIGf5CHAV/deQMN68STEn/hU+9Bp7/vsc7GOcGNcGgciIveJyGoRWSUii0Qkxc9+d4jIOhFZKyKvikiis/5eEcl1jl8lIqPr9gpMpPE/eVUSTRPr+Uwe4HvOEusIN8bDrYGE01W1j6r2AxYA91TdQURSgVuBTFXtBcQDE712+auq9nNe79RF0CZyBZsIqs5ZYpVxjfmBK7ewVPWQ12Ijju/b9JYAJIlIKdAQ2Bnq2Ex0qvyDH8wTUb7mLDHGuNgHIiL3A9cCB4HhVberaq6IPAxsB4qARaq6yGuXm0XkWiAb+K2qHvBznsnAZIBOnTrV7kWYiGKJwJjaFbJbWCLyntN3UfU1BkBV71LVjsDLwM0+jm8OjAG6AilAIxG52tn8JNAd6AfkAY/4i0NVZ6hqpqpmtm7dujYv0RhjYlrIWiCqOjLAXV8B3gb+WGX9SOA7Vc0HEJE5wBDgJVXdXbmTiDyNpx/FGGNMHXLrKaw0r8VLgQ0+dtsODBKRhuKZhm4EsN45vr3XfmOBtaGK1RhjjG9u9YFME5F0oALYBtwI4DzO+4yqjlbVz0VkNrACKANWAjOc4x8SkX54Ot+3AjfUbfjGGGNiaiS6iOTjSViRqhWw1+0gXBLL1w6xff2xfO0QHtffWVVP6ESOqQQS6UQk21c5gVgQy9cOsX39sXztEN7XbzMSGmOMCYolEGOMMUGxBBJZZpx8l6gVy9cOsX39sXztEMbXb30gxhhjgmItEGOMMUGxBGKMMSYolkDClIgki8hsEdkgIutFZLCItBCRxSKy2fnZ3O04Q0FE0r3melklIodE5PYYuv4T5sGJoWu/zbnudSJyu7Muaq9dRJ4TkT0istZrnd/rFZGpIvKNiGwUkVHuRP0DSyDh6zHgXVXtCfTFU8blTmCJqqYBS5zlqKOqGyvnegEGAIXAXGLg+quZBycWrr0XcD0wEM/v/CVO2aNovvYXgAurrPN5vSJyBp7fhTOdY54QkXhcZAkkDIlIU2AY8CyAqpaoagGe6sQznd1mAlluxFfHRgBbVHUbsXP9lfPgJPDDPDixcO2nA8tUtVBVy4CP8NS6i9prV9WPgf1VVvu73jHALFUtVtXvgG/wJFvXWAIJT92AfOB5EVkpIs+ISCOgrarmATg/27gZZB2ZCLzqvI/661fVXKByHpw84KAzD07UXzueoqjDRKSliDQERgMdiY1r9+bvelOBHV775TjrXGMJJDwlAP2BJ1U1AzhKdDXbAyIi9fFUa37D7VjqyknmwYlqqroeeBBYDLwLfIWnkKrxEB/rXB2HYQkkPOUAOar6ubM8G09C2V1Zyt75ucel+OrKRcAKr/lfYuH6v58HR1VLgcp5cGLh2lHVZ1W1v6oOw3NrZzMxcu1e/F1vDp4WWaUOuDzNtyWQMKSqu4AdTsl78PQDfA28BUxy1k0C5rsQXl26gh9uX0FsXL+/eXBi4doRkTbOz07AODz//jFx7V78Xe9bwEQRaSAiXYE04AsX4vuejUQPU858J88A9YFvgZ/hSfivA53w/KH5qapW7YCLCs498B1AN1U96KxrSQxcv4j8CbicH+bB+SXQmNi49v8CLYFS4DequiSa/91F5FXgx3hKtu/GMzPrPPxcr4jcBfwcz+/G7ar6n7qP+geWQIwxxgTFbmEZY4wJiiUQY4wxQbEEYowxJiiWQIwxxgTFEogxEUZELhaR1c4TOca4xhKIMZHnZ3gGWQ53OxAT2yyBmJglIh9WLYntlI1/oppjtopIq9BHd9w5Z4tIN69VC/CMSt7hZ/+nROSoiJxXZf2NIrLGKZH/iVPdFRFpLSLvhuwCTNSyBGJi2at4ijV68y7e6DoROROIV9VvvVY3Bv4LNPOx/x+A5sDZwOMi0sdr8yuq2tspk/8Q8CiAquYDeSIyNDRXYaKVJRATy2bjmXOiAYCIdMFTwPATEbnC+ba+VkQerHqgiHSpMgnQ/4jIvc77D0XkryLysXgmAztLROY4EwT9xeuYq0XkC6dF8JSfuR2u4sTSHVcBNwEZVSYbmgT0Aq5U1bV4ClE+LSIdAVT1kNdnNOL4QnzznM81JmCWQEzMUtV9eGoJVU7oMxF4DWiPpyrseUA/4CwRyarhx5c4BQH/iScB/BrPH/frnHLlp+MpVzLUaRGU4/sP+FBgeeWCiJwGNFDVdc7nTvC6npmqOtGZSwNV3ayqZ6vqDq/jfy0iW/C0QG71Ok828KMaXqOJcZZATKzzvo1VefvqLOBDpyJuGfAyngm+auIt5+caYJ2q5qlqMZ66Zh3xFEkcAHwpIquc5W4+Pqc9nrlhKl0FzPKKvUatBlV9XFW7A/8L/MFr0x48rS9jApbgdgDGuGwe8KiI9AeSVHWFUwn2ZMo4/gtYYpXtxc7PCq/3lcsJeOZ2mKmqU09ynqIqn30lnjlCfuUstxeRjt6tjADNAp70Wk50zmVMwKwFYmKaqh4BPgSe44fO88+Bc0WkldMvcQWe6VW97QbaOLejGgCX1PDUS4AJXuXLW4hIZx/7rQdOc/Y5G9irqh1UtYuqdsFzq+2KQE7ozC9e6WI8c21U6oFnRkBjAmYJxBhP4uiLc2vImUZ0KvABnlnxVqjqcR3ZzmRPf8aTbBYAG2pyQlX9Gs8tpEUishrPLHztfez6Np5y3+C5XTW3yva5QKAzFt4sIuucW2a/4Yc5J8AzpuTtAD/HGMDKuRsT1kQkCU8iG6qq5SE8z8fAGFU9EKpzmOhjCcSYMOcMdlyvqttD9Pmt8SSoeaH4fBO9LIEYY4wJivWBGGOMCYolEGOMMUGxBGKMMSYolkCMMcYExRKIMcaYoFgCMcYYE5T/D/NWmhFAwVrVAAAAAElFTkSuQmCC\n",
      "text/plain": [
       "<Figure size 432x288 with 1 Axes>"
      ]
     },
     "metadata": {
      "needs_background": "light"
     },
     "output_type": "display_data"
    }
   ],
   "source": [
    "%matplotlib inline\n",
    "import numpy as np\n",
    "import matplotlib.pyplot as plt\n",
    "\n",
    "energies = []\n",
    "volumes = []\n",
    "\n",
    "structure_copy = structure.copy()\n",
    "original_cell = structure.cell.copy()\n",
    "\n",
    "structure_copy.calc = calc\n",
    "\n",
    "for scale in np.arange(0.9, 1.11, 0.01):\n",
    "    structure_copy.set_cell(scale * original_cell, scale_atoms=True)\n",
    "    volumes.append(structure_copy.get_volume())\n",
    "    energies.append(structure_copy.get_potential_energy() / len(structure_copy))\n",
    "\n",
    "fig, ax = plt.subplots()\n",
    "ax.plot(volumes, energies, '-o')\n",
    "ax.set_xlabel('Volume (Å^3)')\n",
    "ax.set_ylabel('Energy (eV/atom)');"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "a5ed3304-a3b0-4ed8-9e1d-166355ab2a74",
   "metadata": {
    "tags": []
   },
   "source": [
    "## GPU-based calculator\n",
    "\n",
    "The basic usage of the `GPUNEP` calculator class is completely analoguous to the `CPUNEP` calculator.\n",
    "We create an atomic structure and attach the calculator object."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "id": "6e0bc508-b926-4312-a3ef-43a2f6733be4",
   "metadata": {},
   "outputs": [],
   "source": [
    "from calorine.calculators import GPUNEP\n",
    "\n",
    "structure = bulk('PbTe', crystalstructure='rocksalt', a=6.7)\n",
    "calc = GPUNEP('PbTe_NEP3.txt')\n",
    "structure.calc = calc"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "0daccfc5-91ce-4295-b3b5-eb7ab2a1be7c",
   "metadata": {},
   "source": [
    "Afterwards we can readily obtain energies, forces, and stresses."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "id": "86a7f3fb-3d7f-4b6c-b47c-d4203ad8a739",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Energy (eV): -7.6810722351\n",
      "Forces (eV/Å):\n",
      " [[-4.00012e-08 -4.61735e-08 -3.31625e-08]\n",
      " [ 4.00012e-08  4.61735e-08  3.31625e-08]]\n",
      "Stresses (eV/Å^3):\n",
      " [ 4.82120785e-03  4.82120666e-03  4.82120760e-03 -1.29963668e-09\n",
      " -1.42977897e-09 -1.71918525e-09]\n"
     ]
    }
   ],
   "source": [
    "print('Energy (eV):', structure.get_potential_energy())\n",
    "print('Forces (eV/Å):\\n', structure.get_forces())\n",
    "print('Stresses (eV/Å^3):\\n', structure.get_stress())"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "7ec80392-5346-4da3-9cf1-c556f31a2e37",
   "metadata": {},
   "source": [
    "### Temporary and specified directories\n",
    "\n",
    "Under the hood, the `GPUNEP` calculator creates a directory and writes the input files necessary to run GPUMD.\n",
    "By default, this is done in temporary directories that are automatically removed once the calculations has finished.\n",
    "It is also possible to run in a user-specified directory that will be kept after the calculations finish.\n",
    "This is especially useful when running molecular dynamics simulations (see below)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "id": "be3a38a4-7d18-43ec-b512-37ce7a973223",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "-7.6810722351"
      ]
     },
     "execution_count": 6,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "calc.set_directory('my_directory')\n",
    "structure.get_potential_energy()"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "2eaa6432-df7c-4de5-bb66-f299bf002c6a",
   "metadata": {},
   "source": [
    "After this is run, there should be a new directory, `my_directory`, in which input and output files from GPUMD are available.\n",
    "This can be useful for debugging."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "c0003105-86a3-4253-94c5-26f10c27dbc7",
   "metadata": {},
   "source": [
    "### Running custom molecular dynamics simulations\n",
    "To take advantage of the Python workflow as well as raw speed of the GPU accelerated NEP implementation, the `GPUNEP` calculator contains a convenience function for running customized molecular dynamics simulations.\n",
    "This should typically be done in a specified directory.\n",
    "The [parameters of the run.in file](https://gpumd.zheyongfan.org/index.php/Main_Page#Inputs_for_the_src.2Fgpumd_executable) are specified as a list of tuples with two elements, the first being the keyword name and the second any arguments to that keyword."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "id": "f8e8954f-cebb-4d22-a0b6-670603d03ce9",
   "metadata": {},
   "outputs": [],
   "source": [
    "calc.set_directory('my_md_simulation')\n",
    "supercell = structure.repeat(5)\n",
    "structure.calc = calc\n",
    "parameters = [('velocity', 300),\n",
    "              ('dump_thermo', 100),\n",
    "              ('dump_position', 100),\n",
    "              ('ensemble', ('nvt_lan', 300, 300, 100)),\n",
    "              ('run', 1000)]\n",
    "calc.run_custom_md(parameters)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "f6b6f5e1-c44c-45d1-bb5d-f522393a8cd2",
   "metadata": {},
   "source": [
    "Once this is run, the results are available in the folder `my_md_simulations`."
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.10.4"
  },
  "vscode": {
   "interpreter": {
    "hash": "6be099dd3823815779ed5b2d986ab000320f08fb94733d934e13608f08061690"
   }
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
