This directory contains the source files (`nepy.*`) for building the pybind interface for the `nepy` module.
The CPU only implementation of the NEP was copied from `https://github.com/brucefan1983/NEP_CPU.git` (v1.1, 587ab7c8dcff0a52b0121ddd8827ebbb3e524388).
